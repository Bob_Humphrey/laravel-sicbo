<?php

use Illuminate\Support\Str;

function createLastRollScoringDisplay($score)
{
  return
    '<div>'
    . 'OLD SCORE: '
    . $score['previous_score']
    . '</div><div>'
    . $score['wins']
    . ' '
    . Str::of('bet')->plural($score['wins'])->upper()
    . ' WON ('
    . $score['win_points']
    . ' '
    . Str::of('point')->plural($score['win_points'])->upper()
    . ')</div><div>'
    . $score['losses']
    . ' '
    . Str::of('bet')->plural($score['losses'])->upper()
    . ' LOST ('
    . $score['loss_points']
    . ' '
    . Str::of('point')->plural($score['loss_points'])->upper()
    . ')</div><div>'
    . 'NEW SCORE: '
    . $score['amount']
    . '</div>';
}
