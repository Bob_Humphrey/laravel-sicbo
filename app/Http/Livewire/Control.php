<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Control extends Component
{
  public const DEFAULT_STATUS = 'PLACE YOUR BETS. THEN ROLL THE DICE.';
  public const BET_TOO_HIGH_STATUS = 'YOUR BET IS HIGHER THAN YOUR REMAINING POINTS. REMOVE A BET OR LOWER YOUR BET AMOUNT.';
  public const EMPTY_STATUS = "\u{262E}" . "\u{262E}" . "\u{262E}";
  public const GAME_OVER = 'GAME OVER';
  public const DISABLED = 'DISABLED';

  public $score;
  public $betAmount;
  public $betsPlaced;
  public $status;
  public $lastRollScoringDisplay;
  public $disabledDice;
  public $disabledClearBets;
  public $gameOver;

  // EVENT LISTENERS

  protected $listeners = ['betToggled', 'diceRollCompletedAndEvaluated', 'diceRollStarted'];

  public function betToggled($amount)
  {
    $this->betsPlaced += $amount;
    $this->setButtonState();
  }

  public function diceRollCompletedAndEvaluated($betOutcome)
  {
    $points = $betOutcome * $this->betAmount;
    $this->score['amount'] += $points;

    if ($points > 0) {
      $this->score['wins']++;
      $this->score['win_points'] += $points;
    } elseif ($points < 0) {
      $this->score['losses']++;
      $this->score['loss_points'] += ($points);
    }

    $this->lastRollScoringDisplay = createLastRollScoringDisplay($this->score);

    if ($this->allPointsHaveBeenLost()) {
      $this->gameOver = true;
      $this->disabledDice = self::DISABLED;
      $this->disabledClearBets = '';
      $this->status = self::GAME_OVER;
      $this->emit('betsProhibited');
      return;
    }

    if ($this->notEnoughPointsToCoverBet()) {
      $this->disabledDice = self::DISABLED;
      $this->disabledClearBets = '';
      $this->status = self::BET_TOO_HIGH_STATUS;
      return;
    }

    $this->status = self::EMPTY_STATUS;
  }

  public function diceRollStarted()
  {
    $this->score['previous_score'] = $this->score['amount'];
    $this->score['wins'] = 0;
    $this->score['win_points'] = 0;
    $this->score['losses'] = 0;
    $this->score['loss_points'] = 0;
  }

  // ACTIONS

  public function updateBetAmount($betAmount)
  {
    if ($this->gameOver) {
      return;
    }
    $this->betAmount = $betAmount;
    $this->setButtonState();
  }

  public function betOptionsCleared()
  {
    $this->betsPlaced = 0;
    $this->setButtonState();
    $this->status = self::DEFAULT_STATUS;
    $this->emit('betOptionsCleared');
  }

  public function playAgain()
  {
    $this->init();
  }

  // LIFECYCLE HOOKS

  public function mount()
  {
    $this->init();
  }

  public function render()
  {
    return view('livewire.control');
  }

  // PROTECTED FUNCTIONS

  protected function init()
  {
    $this->score['amount'] = 500;
    $this->score['previous_score'] = 0;
    $this->score['wins'] = 0;
    $this->score['win_points'] = 0;
    $this->score['losses'] = 0;
    $this->score['loss_points'] = 0;
    $this->betAmount = 1;
    $this->betsPlaced = 0;
    $this->status = '';
    $this->lastRollScoringDisplay = '';
    $this->disabledDice = self::DISABLED;
    $this->disabledClearBets = self::DISABLED;
    $this->gameOver = false;
    $this->lastRollScoringDisplay = self::EMPTY_STATUS;
    $this->status = self::DEFAULT_STATUS;
    $this->setButtonState();
    $this->emit('betOptionsCleared');
  }

  protected function setButtonState()
  {
    $this->disabledDice = '';
    $this->disabledClearBets = '';
    $this->status = self::DEFAULT_STATUS;
    if ($this->betsPlaced === 0) {
      $this->disabledDice = self::DISABLED;
      $this->disabledClearBets = self::DISABLED;
      $this->status = self::DEFAULT_STATUS;
    }
    if ($this->score['amount'] < ($this->betAmount * $this->betsPlaced)) {
      $this->disabledDice = self::DISABLED;
      $this->disabledClearBets = '';
      $this->status = self::BET_TOO_HIGH_STATUS;
    }
  }

  protected function notEnoughPointsToCoverBet()
  {
    if ($this->score['amount'] < ($this->betAmount * $this->betsPlaced)) {
      return true;
    }
    return false;
  }

  protected function allPointsHaveBeenLost()
  {
    if ($this->score['amount'] <= 0) {
      return true;
    }
    return false;
  }
}
