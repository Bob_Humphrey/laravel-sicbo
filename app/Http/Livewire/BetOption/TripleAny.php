<?php

namespace App\Http\Livewire\BetOption;

class TripleAny extends BetOption
{
  public $payout = 30;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->distribution['1'] === 3) {
      $this->pay = true;
    } elseif ($this->distribution['2'] === 3) {
      $this->pay = true;
    } elseif ($this->distribution['3'] === 3) {
      $this->pay = true;
    } elseif ($this->distribution['4'] === 3) {
      $this->pay = true;
    } elseif ($this->distribution['5'] === 3) {
      $this->pay = true;
    } elseif ($this->distribution['6'] === 3) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }

  public function render()
  {
    return view('livewire.bet-option.triple-any');
  }
}
