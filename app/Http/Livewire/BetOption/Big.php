<?php

namespace App\Http\Livewire\BetOption;

class Big extends BetOption
{
  public $payout = 1;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }
    $this->pay = $this->big;
    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }

  public function render()
  {
    return view('livewire.bet-option.big');
  }
}
