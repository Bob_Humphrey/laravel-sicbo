<?php

namespace App\Http\Livewire\BetOption;

class TotalTen extends BetOption
{
  public $payout = 6;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->sum === 10) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }
  public function render()
  {
    return view('livewire.bet-option.total-ten');
  }
}
