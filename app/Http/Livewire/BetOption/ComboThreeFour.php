<?php

namespace App\Http\Livewire\BetOption;

class ComboThreeFour extends BetOption
{
  public $payout = 6;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if (($this->distribution['3'] >= 1) && ($this->distribution['4'] >= 1)) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }

  public function render()
  {
    return view('livewire.bet-option.combo-three-four');
  }
}
