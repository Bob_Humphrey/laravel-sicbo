<?php

namespace App\Http\Livewire\BetOption;

class TotalSeventeen extends BetOption
{
  public $payout = 60;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->sum === 17) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }
  public function render()
  {
    return view('livewire.bet-option.total-seventeen');
  }
}
