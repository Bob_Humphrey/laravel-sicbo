<?php

namespace App\Http\Livewire\BetOption;

class ComboTwoThree extends BetOption
{
  public $payout = 6;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if (($this->distribution['2'] >= 1) && ($this->distribution['3'] >= 1)) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }

  public function render()
  {
    return view('livewire.bet-option.combo-two-three');
  }
}
