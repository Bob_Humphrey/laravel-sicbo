<?php

namespace App\Http\Livewire\BetOption;

class TotalSeven extends BetOption
{
  public $payout = 12;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->sum === 7) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }
  public function render()
  {
    return view('livewire.bet-option.total-seven');
  }
}
