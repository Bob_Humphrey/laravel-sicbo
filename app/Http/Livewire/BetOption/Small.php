<?php

namespace App\Http\Livewire\BetOption;

class Small extends BetOption
{
  public $payout = 1;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }
    $this->pay = $this->small;
    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }

  public function render()
  {
    return view('livewire.bet-option.small');
  }
}
