<?php

namespace App\Http\Livewire\BetOption;

class TripleSixes extends BetOption
{
  public $payout = 180;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->distribution['6'] === 3) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }

  public function render()
  {
    return view('livewire.bet-option.triple-sixes');
  }
}
