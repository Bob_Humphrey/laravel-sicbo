<?php

namespace App\Http\Livewire\BetOption;

use Livewire\Component;

class BetOption extends Component
{
  public $dice;
  public $betsAllowed;
  public $diceHaveBeenRolled;
  public $bet;
  public $pay;
  public $distribution = [];
  public $sum;
  public $big;
  public $small;

  // EVENT LISTENERS

  protected $listeners = ['diceRollCompleted', 'betOptionsCleared', 'betsProhibited'];

  public function diceRollCompleted($newDice)
  {
    $this->diceHaveBeenRolled = true;
    $this->pay = false;
    $this->distribution = [
      '1' => 0,
      '2' => 0,
      '3' => 0,
      '4' => 0,
      '5' => 0,
      '6' => 0
    ];

    $this->dice[0] = $newDice[0];
    $this->dice[1] = $newDice[1];
    $this->dice[2] = $newDice[2];
    $this->sum = $this->dice[0] + $this->dice[1] + $this->dice[2];
    foreach ($this->dice as $die) {
      $this->distribution[strval($die)]++;
    }
    $this->big = (($this->sum >= 11) && ($this->sum <= 17)) ? true : false;
    $this->small = (($this->sum >= 4) && ($this->sum <= 10)) ? true : false;
    $betOutcome = $this->determinePay();
    $this->emit('diceRollCompletedAndEvaluated', $betOutcome);
  }

  public function betOptionsCleared()
  {
    $this->init();
  }

  public function betsProhibited()
  {
    $this->betsAllowed = false;
  }

  // ACTIONS

  public function toggleBet()
  {
    if (!$this->betsAllowed) {
      return;
    }
    $this->bet = !$this->bet;
    $betsPlaced = $this->bet ? 1 : -1;
    $this->emit('betToggled', $betsPlaced);
  }

  // This method is overriden by each class that extends BetOption.
  public function determinePay()
  {
  }

  // LIFECYCLE HOOKS

  public function mount()
  {
    $this->init();
  }

  // PROTECTED FUNCTIONS

  protected function init()
  {
    $this->dice = [1, 1, 1];
    $this->betsAllowed = true;
    $this->diceHaveBeenRolled = false;
    $this->bet = false;
    $this->pay = false;
    $this->distribution = [
      '1' => 0,
      '2' => 0,
      '3' => 0,
      '4' => 0,
      '5' => 0,
      '6' => 0
    ];
    $this->sum = 0;
    $this->big = false;
    $this->small = false;
  }
}
