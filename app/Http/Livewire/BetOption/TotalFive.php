<?php

namespace App\Http\Livewire\BetOption;

class TotalFive extends BetOption
{
  public $payout = 30;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->sum === 5) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }
  public function render()
  {
    return view('livewire.bet-option.total-five');
  }
}
