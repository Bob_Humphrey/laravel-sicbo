<?php

namespace App\Http\Livewire\BetOption;

class DoubleSixes extends BetOption
{
  public $payout = 10;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->distribution['6'] === 2) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }
  public function render()
  {
    return view('livewire.bet-option.double-sixes');
  }
}
