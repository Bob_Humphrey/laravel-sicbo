<?php

namespace App\Http\Livewire\BetOption;

class TotalEight extends BetOption
{
  public $payout = 8;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->sum === 8) {
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }
  public function render()
  {
    return view('livewire.bet-option.total-eight');
  }
}
