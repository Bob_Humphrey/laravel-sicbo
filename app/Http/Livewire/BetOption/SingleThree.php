<?php

namespace App\Http\Livewire\BetOption;

class SingleThree extends BetOption
{
  public $payout = 1;

  public function determinePay()
  {
    if (!$this->bet) {
      return 0;
    }

    if ($this->distribution['3'] === 1) {
      $this->payout = 1;
      $this->pay = true;
    } elseif ($this->distribution['3'] === 2) {
      $this->payout = 2;
      $this->pay = true;
    } elseif ($this->distribution['3'] === 3) {
      $this->payout = 3;
      $this->pay = true;
    }

    if ($this->pay) {
      return $this->payout;
    }
    return -1;
  }

  public function render()
  {
    return view('livewire.bet-option.single-three');
  }
}
