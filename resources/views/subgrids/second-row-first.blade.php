<div class="flex flex-1 flex-wrap">

  @livewire('bet-option.double-ones')

  @livewire('bet-option.double-twos')

  @livewire('bet-option.double-threes')

  <div class="flex flex-col w-2/5">
    @livewire('bet-option.triple-ones')
    @livewire('bet-option.triple-twos')
    @livewire('bet-option.triple-threes')
  </div>

</div>
