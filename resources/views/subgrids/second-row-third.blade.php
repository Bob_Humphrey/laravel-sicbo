<div class="flex flex-1 flex-wrap">

  <div class="flex flex-col w-2/5">
    @livewire('bet-option.triple-fours')
    @livewire('bet-option.triple-fives')
    @livewire('bet-option.triple-sixes')
  </div>

  @livewire('bet-option.double-fours')

  @livewire('bet-option.double-fives')

  @livewire('bet-option.double-sixes')

</div>
