<div class="flex items-center justify-center w-full">
  <button wire:click="updateBetAmount(1)" class="btn">
    Bet 1
  </button>
  <button wire:click="updateBetAmount(2)" class="btn">
    Bet 2
  </button>
  <button wire:click="updateBetAmount(3)" class="btn">
    Bet 3
  </button>
  <button wire:click="updateBetAmount(4)" class="btn">
    Bet 4
  </button>
  <button wire:click="updateBetAmount(5)" class="btn">
    Bet 5
  </button>
  <button wire:click="betOptionsCleared" {{ $disabledClearBets }} class="btn">
    Clear Bets
  </button>
</div>
