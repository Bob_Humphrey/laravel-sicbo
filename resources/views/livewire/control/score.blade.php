<div class="flex py-2">

  <div class="flex items-center justify-center mr-6">
    <div class="flex flex-col">
      <div class="text-center">
        POINTS
      </div>
      <div class="text-center">
        {{ $score['amount'] }}
      </div>
    </div>
  </div>

  <div class="flex items-center justify-center">
    <div class="flex flex-col">
      <div class="text-center">
        BET
      </div>
      <div class="text-center">
        {{ $betsPlaced }} X {{ $betAmount }} = {{ $betsPlaced * $betAmount }}
      </div>
    </div>
  </div>

</div>
