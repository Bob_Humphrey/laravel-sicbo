<div class="flex items-center justify-center w-full bg-brown-600 py-2">
  <button wire:click="playAgain" class="btn">
    Play Again
  </button>
</div>
