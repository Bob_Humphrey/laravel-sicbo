<div x-data="
  {
    dice: [1, 1, 1],
    counter: 10,

    roll: function () {
        window.livewire.emit('diceRollStarted');
        this.counter = 15;
        const rollInterval = setInterval(() => {
            this.dice[0] = Math.floor(Math.random() * 6) + 1;
            this.dice[1] = Math.floor(Math.random() * 6) + 1;
            this.dice[2] = Math.floor(Math.random() * 6) + 1;
            this.counter--;
            if (this.counter === 0) {
              clearInterval(rollInterval);
              var dice = [];
              dice.push(this.dice[0]);
              dice.push(this.dice[1]);
              dice.push(this.dice[2]);
              window.livewire.emit('diceRollCompleted', dice);
            }
        }, 60);
    }
  }
">

  <div class="flex">
    <div class="flex self-center mr-2">
      <button x-on:click="roll" {{ $disabledDice }} class="btn">
        Roll Dice
      </button>
    </div>
    <div class="flex">
      <div x-show="dice[0] === 6" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-6', 'fill-current')
      </div>
      <div x-show="dice[0] === 5" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-5', 'fill-current')
      </div>
      <div x-show="dice[0] === 4" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-4', 'fill-current')
      </div>
      <div x-show="dice[0] === 3" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-3', 'fill-current')
      </div>
      <div x-show="dice[0] === 2" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-2', 'fill-current')
      </div>
      <div x-show="dice[0] === 1" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-1', 'fill-current')
      </div>
      <div x-show="dice[1] === 6" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-6', 'fill-current')
      </div>
      <div x-show="dice[1] === 5" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-5', 'fill-current')
      </div>
      <div x-show="dice[1] === 4" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-4', 'fill-current')
      </div>
      <div x-show="dice[1] === 3" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-3', 'fill-current')
      </div>
      <div x-show="dice[1] === 2" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-2', 'fill-current')
      </div>
      <div x-show="dice[1] === 1" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-1', 'fill-current')
      </div>
      <div x-show="dice[2] === 6" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-6', 'fill-current')
      </div>
      <div x-show="dice[2] === 5" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-5', 'fill-current')
      </div>
      <div x-show="dice[2] === 4" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-4', 'fill-current')
      </div>
      <div x-show="dice[2] === 3" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-3', 'fill-current')
      </div>
      <div x-show="dice[2] === 2" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-2', 'fill-current')
      </div>
      <div x-show="dice[2] === 1" class="w-12 overflow-visible bg-brown-600">
        @svg('dice-1', 'fill-current')
      </div>
    </div>
  </div>
</div>
