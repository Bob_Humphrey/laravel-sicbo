<div class="w-full bg-brown-600 text-sm text-center py-2">
  <div class="text-amber-200">
    {{ $status }}
  </div>
  <div class="flex justify-around text-white">
    {!! $lastRollScoringDisplay !!}
  </div>
</div>
