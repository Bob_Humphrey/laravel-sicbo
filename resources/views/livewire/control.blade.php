@php
$showPlay = $gameOver ? 'hidden' : '';
$showPlayAgain = $gameOver ? '' : 'hidden';
@endphp

<div class="">

  <div class="mb-2">
    @include('livewire.control.status')
  </div>

  <div class="{{ $showPlay }} grid grid-cols-12 bg-brown-600 text-white px-6">
    <div class="col-span-2 flex items-center">
      @include('livewire.control.score')
    </div>

    <div class="col-span-7 flex items-center">
      @include('livewire.control.buttons')
    </div>

    <div class="col-span-3 flex items-center">
      @include('livewire.control.dice')
    </div>
  </div>

  <div class={{ $showPlayAgain }}>
    @include('livewire.control.play-again')
  </div>

</div>
