@php
$bgColor = 'bg-brown-600';
if ($bet && !$diceHaveBeenRolled) {
$bgColor = 'bg-amber-600';
}
if ($bet && $pay && $diceHaveBeenRolled) {
$bgColor = 'bg-green-600';
}
if ($bet && !$pay && $diceHaveBeenRolled) {
$bgColor = 'bg-red-600';
}
@endphp

<div wire:click="toggleBet" class="{{ $bgColor }} flex-1 text-white py-2 cursor-pointer">
  <div class="text-3xl text-center">
    BIG
  </div>
  <div class="text-xs text-center">
    11 to 17 - 1 wins {{ $payout }}
  </div>
</div>

{{-- <div class="grid grid-cols-12">
  <div class="col-span-4">
    SUM
  </div>
  <div class="col-span-4">
    {{ $sum }}
  </div>
</div>

<div class="grid grid-cols-12">
  <div class="col-span-4">
    BET
  </div>
  <div class="col-span-4">
    {{ $bet }}
  </div>
</div>

<div class="grid grid-cols-12">
  <div class="col-span-4">
    PAY
  </div>
  <div class="col-span-4">
    {{ $pay }}
  </div>
</div>

<div class="grid grid-cols-12">
  <div class="col-span-4">
    ROLL
  </div>
  <div class="col-span-4">
    {{ $diceHaveBeenRolled }}
  </div>
</div> --}}
