@php
$bgColor = 'bg-brown-600';
if ($bet && !$diceHaveBeenRolled) {
$bgColor = 'bg-amber-600';
}
if ($bet && $pay && $diceHaveBeenRolled) {
$bgColor = 'bg-green-600';
}
if ($bet && !$pay && $diceHaveBeenRolled) {
$bgColor = 'bg-red-600';
}
@endphp

<div wire:click="toggleBet" class="{{ $bgColor }} flex-col w-full text-white cursor-pointer py-1">
  <div class="flex flex-col">
    <div class="flex justify-center">
      <div class="w-8 overflow-visible">
        @svg('dice-5', 'fill-current')
      </div>
      <div class="w-8 overflow-visible">
        @svg('dice-5', 'fill-current')
      </div>
      <div class="w-8 overflow-visible">
        @svg('dice-5', 'fill-current')
      </div>
    </div>
    <div class="text-xs text-center">
      1 wins {{ $payout }}
    </div>
  </div>
</div>
