@php
$bgColor = 'bg-brown-600';
if ($bet && !$diceHaveBeenRolled) {
$bgColor = 'bg-amber-600';
}
if ($bet && $pay && $diceHaveBeenRolled) {
$bgColor = 'bg-green-600';
}
if ($bet && !$pay && $diceHaveBeenRolled) {
$bgColor = 'bg-red-600';
}
@endphp

<div wire:click="toggleBet" class="{{ $bgColor }} flex items-center justify-center  text-white py-2 cursor-pointer">
  <div class="flex flex-col">
    <div class="font-nunito_bold text-4xl text-center">
      10
    </div>
    <div class="text-xs text-center font-nunito_regular pt-1">
      1 wins {{ $payout }}
    </div>
  </div>
</div>
