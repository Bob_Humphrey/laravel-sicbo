@php
$bgColor = 'bg-brown-600';
if ($bet && !$diceHaveBeenRolled) {
$bgColor = 'bg-amber-600';
}
if ($bet && $pay && $diceHaveBeenRolled) {
$bgColor = 'bg-green-600';
}
if ($bet && !$pay && $diceHaveBeenRolled) {
$bgColor = 'bg-red-600';
}
@endphp

<div wire:click="toggleBet" class="{{ $bgColor }} flex items-center justify-center text-white cursor-pointer py-2">
  <div class="flex flex-col">
    <div class="w-12 overflow-visible self-center">
      @svg('dice-1', 'fill-current')
    </div>
    <div class="text-xs text-center pt-1">
      1 wins 1 / 2 wins 2 / 3 wins 3
    </div>
  </div>
</div>
