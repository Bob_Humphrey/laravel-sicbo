@php
$bgColor = 'bg-brown-300';
if ($bet && !$diceHaveBeenRolled) {
$bgColor = 'bg-amber-600';
}
if ($bet && $pay && $diceHaveBeenRolled) {
$bgColor = 'bg-green-600';
}
if ($bet && !$pay && $diceHaveBeenRolled) {
$bgColor = 'bg-red-600';
}
@endphp

<div wire:click="toggleBet"
  class="{{ $bgColor }} flex items-center justify-center w-full text-white cursor-pointer py-1">
  <div class="flex flex-col">
    <div class="text-center pb-2">
      Any Triple
    </div>
    <div class="flex justify-center">
      <div class="flex flex-col justify-center">
        <div class="w-8 overflow-visible">
          @svg('dice-1', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-1', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-1', 'fill-current')
        </div>
      </div>
      <div class="flex flex-col justify-center">
        <div class="w-8 overflow-visible">
          @svg('dice-2', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-2', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-2', 'fill-current')
        </div>
      </div>
      <div class="flex flex-col justify-center">
        <div class="w-8 overflow-visible">
          @svg('dice-3', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-3', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-3', 'fill-current')
        </div>
      </div>
      <div class="flex flex-col justify-center">
        <div class="w-8 overflow-visible">
          @svg('dice-4', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-4', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-4', 'fill-current')
        </div>
      </div>
      <div class="flex flex-col justify-center">
        <div class="w-8 overflow-visible">
          @svg('dice-5', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-5', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-5', 'fill-current')
        </div>
      </div>
      <div class="flex flex-col justify-center">
        <div class="w-8 overflow-visible">
          @svg('dice-6', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-6', 'fill-current')
        </div>
        <div class="w-8 overflow-visible">
          @svg('dice-6', 'fill-current')
        </div>
      </div>
    </div>
    <div class="text-xs text-center pt-1">
      1 wins 30
    </div>
  </div>
</div>
