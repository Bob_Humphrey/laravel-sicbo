<div class="container bg-brown-50 font-nunito_regular mx-auto px-16 pt-1 pb-1 mt-1 mb-4" x-show="show === 'game'">

  <!-- BIG / SMALL -->
  <div class="-mx-1 flex flex-wrap">

    <div class="flex flex-col p-1 w-1/3">
      @livewire('bet-option.small')
    </div>

    <div class="flex flex-col p-1 w-1/3 ">
      <div class="text-5xl font-nunito_bold text-brown-600 text-center leading-tight mx-5">
        {{ config('app.name', 'Laravel Application') }}
      </div>
      <div class="flex items-center w-full">
        <button class="bg-brown-300 text-amber-200 text-center font-nunito_bold rounded w-48 py-1 mx-auto"
          x-on:click="show = 'help'">
          HOW TO PLAY
        </button>
      </div>
    </div>

    <div class="flex flex-col p-1 w-1/3">
      @livewire('bet-option.big')
    </div>
  </div>

  <!-- DOUBLES AND TRIPLES -->
  <div class="-mx-1 flex flex-wrap">

    <div class="flex flex-col p-1 w-1/3">
      @include('subgrids.second-row-first')
    </div>

    <div class="flex flex-col p-1 w-1/3">
      @include('subgrids.second-row-second')
    </div>

    <div class="flex flex-col p-1 w-1/3">
      @include('subgrids.second-row-third')
    </div>
  </div>

  <!-- TOTALS -->
  <div class="-mx-1 flex flex-wrap">

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-four')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-five')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-six')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-seven')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-eight')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-nine')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-ten')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-eleven')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-twelve')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-thirteen')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-fourteen')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-fifteen')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-sixteen')
    </div>

    <div class="flex flex-col p-1 w-1/14">
      @livewire('bet-option.total-seventeen')
    </div>
  </div>

  <!-- TOTALS -->
  <div class="-mx-1 flex flex-wrap">

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-one-two')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-one-three')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-one-four')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-one-five')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-one-six')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-two-three')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-two-four')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-two-five')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-two-six')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-three-four')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-three-five')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-three-six')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-four-five')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-four-six')
    </div>

    <div class="flex flex-col p-1 w-1/15">
      @livewire('bet-option.combo-five-six')
    </div>
  </div>

  <!-- SINGLES -->
  <div class="-mx-1 flex flex-wrap">

    <div class="flex flex-col p-1 w-1/6">
      @livewire('bet-option.single-one')
    </div>

    <div class="flex flex-col p-1 w-1/6">
      @livewire('bet-option.single-two')
    </div>

    <div class="flex flex-col p-1 w-1/6">
      @livewire('bet-option.single-three')
    </div>

    <div class="flex flex-col p-1 w-1/6">
      @livewire('bet-option.single-four')
    </div>

    <div class="flex flex-col p-1 w-1/6">
      @livewire('bet-option.single-five')
    </div>

    <div class="flex flex-col p-1 w-1/6">
      @livewire('bet-option.single-six')
    </div>
  </div>

  <div class="mt-1">
    @livewire('control')
  </div>
</div>
