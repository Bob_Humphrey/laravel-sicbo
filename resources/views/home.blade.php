@extends('layouts.app')

@section('content')

  <div x-data="{ show : 'game' }">

    @include('game')

    @include('help')

  </div>

@endsection
