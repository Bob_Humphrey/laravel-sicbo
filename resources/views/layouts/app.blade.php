<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="Sic Bo is a gambling game implemented with the TALL stack - Tailwind CSS, Alpine Js, Laravel and Livewire.">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  @livewireScripts
  <script src="{{ asset('js/all.js') }}" defer></script>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body class="bg-white">
  <div class="font-nunito_regular">

    <main class="">
      @yield('content')
    </main>

    @include('layouts.footer')

  </div>
</body>

</html>
