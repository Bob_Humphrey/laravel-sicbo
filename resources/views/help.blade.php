  <div class="container w-lg bg-white font-nunito_regular text-lg mx-auto px-64 my-4 py-6" x-show="show === 'help'">

    <div class="flex justify-center w-full mb-6">
      <button class="text-amber-900" x-on:click="show = 'game'">
        Return to the game
      </button>
    </div>

    <h2 class="text-4xl font-nunito_bold text-brown-600 text-center leading-tight mb-6">
      How to Play Sic Bo
    </h2>

    <p class="">
      Sic Bo is a simple gambling game where the player bets on the outcome of rolling 3 dice.
    </p>

    <p class="mt-6">
      The game proceeds as follows:
    </p>

    <ul class="mt-6 list-inside list-disc">
      <li class="">
        The player places his bets.
      </li>
      <li class="">
        The player rolls the dice.
      </li>
      <li class="">
        The score is adjusted based on the bets placed and the roll of the dice.
      </li>
      <li class="">
        Repeat the above steps.
      </li>
    </ul>

    <p class="mt-6">
      At the beginning of the game the board looks like this.
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game1.png" alt="Sic Bo board at the beginning of the game">
    </div>

    <p class="mt-6">
      <span class="font-nunito-bold text-amber-900">
        Step 1: Place your bets.
      </span>
      There are 50 choices on the board, each representing a possible outcome from rolling the dice. Click on a choice
      to place your bet. If you change your mind, click on it again and your bet will be removed. Place as many bets as
      you'd like.
    </p>

    <p class="mt-6">
      When you place a bet, your bet lights up in amber.
    </p>

    <p class="mt-6">
      The player wins when the rolled dice match their bet. In the game shown below, the player would win if the rolled
      dice contained on or more of the following:
    </p>

    <ul class="list-inside list-disc mt-6">
      <li>Double threes</li>
      <li>Double fours</li>
      <li>One and three</li>
      <li>One and four</li>
      <li>Four and five</li>
      <li>Four and six</li>
      <li>Three</li>
      <li>Four</li>
    </ul>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game1a.png" alt="Sic Bo board with bets placed">
    </div>

    <p class="mt-6">
      The SMALL bet pays when the total of all three dice is from 4 to 10.
    </p>

    <p class="mt-6">
      The BIG bet pays when the total of all three dice is from 11 to 17.
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game1b.png" alt="Sic Bo board with bets placed">
    </div>

    <p class="mt-6">
      There are fourteen squares across the middle of the board labeled 4 to 17. Betting on any of these wins if the
      total of all three dice matches the bet. The example below would pay if the total equals 8, 10 or 12.
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game1c.png" alt="Sic Bo board with bets placed">
    </div>

    <p class="mt-6">
      The payoff for each bet is shown along with your bet. For example, a winning bet on one of the doubles returns 10
      points for every point bet. A winning bet on one of the triples returns 180 points for every point bet.
    </p>

    <p class="mt-6">
      In our example game, we've placed 12 bets and the board looks like this:
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game1x.png" alt="Sic Bo board with bets placed">
    </div>

    <p class="mt-6">
      <span class="font-nunito-bold text-amber-900">
        Step 2: Roll the dice.
      </span>
      The button is in the bottom right corner, next to the dice.
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game2.png" alt="Sic Bo board with button for rolling the dice">
    </div>

    <p class="mt-6">
      <span class="font-nunito-bold text-amber-900">
        Step 3: See the results.
      </span>
      Winning bets turn green, and losing bets are red.
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game3.png" alt="Sic Bo board showing winning and losing bets">
    </div>

    <p class="mt-6">
      <span class="font-nunito-bold text-amber-900">
        Check your new score.
      </span>
      Your new score is in the bottom left corner. The line above shows how your scrore was calculated, based on your
      wagers and the payoffs from your winning bets.
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game3a.png" alt="Sic Bo board showing winning and losing bets">
    </div>

    <p class="mt-6">
      <span class="font-nunito-bold text-amber-900">
        Place your next bets.
      </span>
      When the game starts, you are wagering 1 point for each bet you place. But you can increase your bet using the
      change bet buttons: Bet 2, Bet 3, Bet 4, or Bet 5.
    </p>

    <p class="mt-6">
      If you want to keep your bets from the last round, then just click Roll the Dice.
    </p>

    <p class="mt-6">
      If you want to start all over with new bets, click Clear Bets.
    </p>

    <p class="mt-6">
      Or you can click individual boxes to add and remove bets one at a time.
    </p>

    <div class="w-2/3 mx-auto mt-6">
      <img src="/img/game3b.png" alt="Sic Bo board showing winning and losing bets">
    </div>

    <div class="flex justify-center w-full mt-6">
      <button class="text-amber-900" x-on:click="show = 'game'">
        Return to the game
      </button>
    </div>

  </div>
